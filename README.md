# GildedRose

The repository contains a simple Java application named GildedRose which update items in a store and is accompanied by a unit test to check that the main application works as expected.

You can have a full description of the project here: https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt

The original code can be found here: https://github.com/emilybache/GildedRose-Refactoring-Kata

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## System Requirements

You need to have :
* Maven 2 or above (installation guide: https://github.com/apache/maven/blob/master/apache-maven/README.txt)
* JDK 1.8 or above (installation guide: https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)

Operating System:
Windows or Unix based systems

## Installing GildedRose

First you need to download the project via HTTPS on gitlab.com:
https://gitlab.com/timotmechler/gildedrose.git

**For Linux**:
 ```
 mkdir -p /home/user/projects/git
 cd /home/user/projects/git
 ```

 
**For Windows** (create directory first!):
 ``` 
 cd /c/user/projects/git 
 ```

Then:
 ``` 
 git clone https://gitlab.com/timotmechler/gildedrose.git 
 ```
 
Then you can run the unit test like this:

 ``` 
 mvn test 
 ```

## Additional information

The approach was:

* Fixing and extending unit test with thoroughly test cases (TDD). This part was a very helpful move (even if it was quite time-consuming).
* Adding an extra unit test for 'Conjured item' feature (with @Ignored as the feature was not implemented yet) 
* Refactoring the core code and running the test again until all test passed.
* Implementing the 'Conjured item' feature and turning its dedicated test on.

Note: each step was in a dedicated commit, but unfortunately went in an 'initial commit' during git repo creation (I did not well plan this, I should have thought ahead and created this repo first!).