package com.gildedrose;

import static com.gildedrose.GildedRose.AGED_BRIE;
import static com.gildedrose.GildedRose.BACKSTAGE_PASS;
import static com.gildedrose.GildedRose.CONJURED;
import static com.gildedrose.GildedRose.DEXTERITY_VEST;
import static com.gildedrose.GildedRose.ELEXIR_OF_MONGOOSE;
import static com.gildedrose.GildedRose.SULFURAS;
import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

/**
 * Test class for GildedRose.
 *
 * @author mechlert
 */
public class GildedRoseTest {

    /**
     * Object under test.
     */
    private GildedRose gildedRose;

    /**
     * Setting up test data.
     */
    @Before
    public void setUp() {

        //items creation
        Item dexterityVest = new Item(DEXTERITY_VEST, 10, 20);
        Item agedBrie = new Item(AGED_BRIE, 2, 0);
        Item elixirOfTheMongoose = new Item(ELEXIR_OF_MONGOOSE, 5, 7);
        Item sulfurasFirst = new Item(SULFURAS, 0, 80);
        Item sulfurasSecond = new Item(SULFURAS, -1, 80);
        Item backStageFirst = new Item(BACKSTAGE_PASS, 15, 20);
        Item backStageSecond = new Item(BACKSTAGE_PASS, 10, 49);
        Item backStageThird = new Item(BACKSTAGE_PASS, 5, 49);
        Item conjuredManaCake = new Item(CONJURED, 3, 6);

        Item[] items = new Item[]{
            dexterityVest,
            agedBrie,
            elixirOfTheMongoose,
            sulfurasFirst,
            sulfurasSecond,
            backStageFirst,
            backStageSecond,
            backStageThird,
            conjuredManaCake
        };
        
        gildedRose = new GildedRose(items);
    }

    @Test
    public void testSimpleItemOverTime() {
        //quality decrease by one
        gildedRose.dailyItemsUpdate();
        assertEquals(DEXTERITY_VEST, gildedRose.items[0].name);
        assertEquals(9, gildedRose.items[0].sellIn);
        assertEquals(19, gildedRose.items[0].quality);

        for (int i = 0; i < 9; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(0, gildedRose.items[0].sellIn);
        assertEquals(10, gildedRose.items[0].quality);

        //quality decreases twice as fast when item's sellIn day is negative
        gildedRose.dailyItemsUpdate();
        assertEquals(-1, gildedRose.items[0].sellIn);
        assertEquals(8, gildedRose.items[0].quality);

        //quality don't go under 0
        for (int i = 0; i < 5; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(-6, gildedRose.items[0].sellIn);
        assertEquals(0, gildedRose.items[0].quality);

    }

    @Test
    public void testSulfureItemOverTime() {
        for (int i = 0; i < 40; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(-1, gildedRose.items[4].sellIn);
        assertEquals(80, gildedRose.items[4].quality);
    }

    @Test
    public void testAgedBrieItemOverTime() {
        gildedRose.dailyItemsUpdate();
        assertEquals(1, gildedRose.items[1].sellIn);
        assertEquals(1, gildedRose.items[1].quality);

        gildedRose.dailyItemsUpdate();
        assertEquals(0, gildedRose.items[1].sellIn);
        assertEquals(2, gildedRose.items[1].quality);

        //quality goes over with time
        gildedRose.dailyItemsUpdate();
        assertEquals(-1, gildedRose.items[1].sellIn);
        assertEquals(4, gildedRose.items[1].quality);

        //quality cannot go over 50
        for (int i = 0; i < 100; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(-101, gildedRose.items[1].sellIn);
        assertEquals(50, gildedRose.items[1].quality);
    }

    @Test
    public void testBackstageItemOverTime() {

        //more than 10 day left
        for (int i = 0; i < 5; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(10, gildedRose.items[5].sellIn);
        assertEquals(25, gildedRose.items[5].quality);

        //less than 10 day left but more than 5
        for (int i = 0; i < 5; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(5, gildedRose.items[5].sellIn);
        assertEquals(35, gildedRose.items[5].quality);

        //less than 5 day left but more than 0
        for (int i = 0; i < 5; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(0, gildedRose.items[5].sellIn);
        assertEquals(50, gildedRose.items[5].quality);

        //timeout -> ticket lost all their value
        gildedRose.dailyItemsUpdate();
        assertEquals(-1, gildedRose.items[5].sellIn);
        assertEquals(0, gildedRose.items[5].quality);

    }

    @Test
    public void testConjuredItemOverTime() {
        for (int i = 0; i < 2; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(1, gildedRose.items[8].sellIn);
        assertEquals(2, gildedRose.items[8].quality);

        //quality cannot go under 0
        for (int i = 0; i < 2; i++) {
            gildedRose.dailyItemsUpdate();
        }
        assertEquals(-1, gildedRose.items[8].sellIn);
        assertEquals(0, gildedRose.items[8].quality);
    }

}
