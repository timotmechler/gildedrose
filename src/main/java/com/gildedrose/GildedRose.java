package com.gildedrose;

/**
 * GildeRose items store managment.
 *
 * @author mechlert
 */
class GildedRose {

    public static final String DEXTERITY_VEST = "+5 Dexterity Vest";
    public static final String AGED_BRIE ="Aged Brie";
    public static final String ELEXIR_OF_MONGOOSE = "Elixir of the Mongoose";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String BACKSTAGE_PASS = "Backstage passes to a TAFKAL80ETC concert";
    public static final String CONJURED = "Conjured Mana Cake";

    public static final int MIN_QUALITY = 0;
    public static final int MAX_QUALITY = 50;

    public static final int NORMAL_QUALITY_DECREASE = -1;
    public static final int TWO_TIMES_QUALITY_DECREASE = -2;
    public static final int FOUR_TIMES_QUALITY_DECREASE = -4;

    public static final int NORMAL_QUALITY_INCREASE = 1;
    public static final int TWO_TIMES_QUALITY_INCREASE = 2;
    public static final int THREE_TIMES_QUALITY_INCREASE = 3;

    /**
     * Store items.
     */
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    /**
     * Daily update of all items of GildeRose.
     */
    public void dailyItemsUpdate() {
        for (Item item : items) {
            updateSellInFor(item);
            updateQualityFor(item);
        }
    }

    /**
     * Update sellIn for a given item.
     *
     * @param item the item to udpate
     */
    private void updateSellInFor(Item item) {
        if ( ! item.name.equals(SULFURAS)) {
            item.sellIn--;
        }
    }

    /**
     * Update quality for a given item.
     *
     * @param item
     */
    private void updateQualityFor(Item item) {
        switch(item.name) {
           case AGED_BRIE:
               updateQualityForBrie(item);
               break;
           case SULFURAS:
               //nothing to do
               break;
           case BACKSTAGE_PASS:
               updateQualityForBackstage(item);
               break;
           case CONJURED:
               updateQualityForConjured(item);
               break;
           default:
               updateQualityDefault(item);
               break;

        }
    }

    /**
     * Brie item's quality update.
     *
     * @param item the item to udpate
     */
    private void updateQualityForBrie(Item item) {
        int newQualityInc = item.sellIn >= 0 ? NORMAL_QUALITY_INCREASE : TWO_TIMES_QUALITY_INCREASE;
        updateQualityBy(item, newQualityInc);
    }

    /**
     * Backstage item's quality update.
     *
     * @param item the item to udpate
     */
    private void updateQualityForBackstage(Item item) {
        if (item.sellIn < 0) {
            item.quality = MIN_QUALITY;
        } else {
            int newQualityInc = NORMAL_QUALITY_INCREASE;
            if (item.sellIn < 5) {
               newQualityInc = THREE_TIMES_QUALITY_INCREASE;
            } else if(item.sellIn < 10) {
               newQualityInc = TWO_TIMES_QUALITY_INCREASE;
            }
            updateQualityBy(item, newQualityInc);
        }
    }

    /**
     * Conjured item's quality update.
     *
     * @param item the item to udpate
     */
    private void updateQualityForConjured(Item item) {
        int newQualityInc = item.sellIn >= 0 ? TWO_TIMES_QUALITY_DECREASE : FOUR_TIMES_QUALITY_DECREASE;
        updateQualityBy(item, newQualityInc);
    }

    /**
     * standard item's quality update.
     *
     * @param item the item to udpate
     */
    private void updateQualityDefault(Item item) {
        int newQualityInc = item.sellIn >= 0 ? NORMAL_QUALITY_DECREASE : TWO_TIMES_QUALITY_DECREASE;
        updateQualityBy(item, newQualityInc);
    }

    /**
     * Update quality taking into account the upper and lower quality limit.
     *
     * @param item the item to udpate
     * @param newQualityInc the value to increment or decrement (can be negative)
     */
    private void updateQualityBy(Item item, int newQualityInc) {
        item.quality = Math.max(MIN_QUALITY, Math.min(MAX_QUALITY, item.quality + newQualityInc));
    }
}